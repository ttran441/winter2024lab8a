public class Board {
	
	private Tile[][] grid;
	private final int ROWS;
	private final int COLUMNS;
	
	public Board() {
		
		ROWS = 3;
		COLUMNS = 3;
		
		this.grid = new Tile[ROWS][COLUMNS];
		
		intializeGrid();
		
	}
	
	// Method to initialize the grid
	private void intializeGrid() {
		
		for(int i = 0; i < this.grid.length; i++) {
			
			for(int j = 0; j < this.grid[i].length; j++) {
				
				this.grid[i][j] = Tile.BLANK;
				
			}
			
		}
		
	}
	
	// Prints the grid
	public String toString() {
		
		String result = "";
		
		for(Tile[] row : this.grid) {
			
			result += "\n";
			
			for(Tile col : row) {
				
				result += "[" + col.getName() + "] ";
				
			}
			
			result += "\n";
			
		}
		
		return result;
		
	}
	
	// If the placement of the token is invalid, it returns false, if not, it places the token.
	public boolean placeToken(int row, int col, Tile playerToken) {
		
		if(row < 0 || row > ROWS-1) {
			
			return false;
			
		}
		
		else if(col < 0 || col > COLUMNS-1) {
			
			return false;
			
		}
		
		if(this.grid[row][col] == Tile.BLANK) {
			
			this.grid[row][col] = playerToken;
			
			return true;
			
		}
		
		return false;
		
	}
	
	// Checks for draw
	public boolean checkIfFull() {
		
		for(Tile[] row : this.grid) {

			for(Tile col : row) {
				
				if(col == Tile.BLANK) {
					
					return false;
					
				}
				
			}
			
		}
		
		return true;
		
	}
	
	// Checks every horizontal line for a winner
	private boolean checkIfWinningHorizontal(Tile playerToken) {
		
		for(int i = 0; i < this.grid.length; i++) {
			
			if(this.grid[i][0] == playerToken && this.grid[i][1]  == playerToken && this.grid[i][2] == playerToken) {
				
				return true;
				
			}
			
		}
		
		return false;
		
	}
	
	// Checks every vertical line for a winner
	private boolean checkIfWinningVertical(Tile playerToken) {
		
		for(int i = 0; i < this.grid.length; i++) {
			
			if(this.grid[0][i] == playerToken && this.grid[1][i] == playerToken && this.grid[2][i] == playerToken) {
				
				return true;
				
			}
			
		}
		
		return false;
		
	}
	
	// Checks both diagonal
	private boolean checkIfWinningDiagonal(Tile playerToken) {
		
		return (this.grid[0][0] == playerToken && this.grid[1][1] == playerToken && this.grid[2][2] == playerToken) || (this.grid[0][2] == playerToken && this.grid[1][1] == playerToken && this.grid[2][0] == playerToken);
		
	}
	
	// Method to check for winner
	public boolean checkIfWinning(Tile playerToken) {
		
		return checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken);
		
	}
	
}