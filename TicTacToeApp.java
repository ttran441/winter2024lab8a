import java.util.Scanner;

public class TicTacToeApp {
	
	public static void main(String[] args) {
		
		System.out.println("Welcome to Tic-Tac-Toe!");
		
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Tile playerToken = Tile.X;
		
		Scanner sc = new Scanner(System.in);
		
		// Loop to run game
		while(!gameOver) {
			
			System.out.println(board);
			
			System.out.println("Player " + player + ":");
			
			System.out.println("Please enter a row (1-3): ");
			int row = Integer.parseInt(sc.nextLine()) - 1;
			
			System.out.println("Please enter a column (1-3): ");
			int col = Integer.parseInt(sc.nextLine()) - 1;
			
			if(player == 1) {
				
				playerToken = Tile.X;
				
			}
			
			else {
				
				playerToken = Tile.O;
				
			}
			
			boolean placed = board.placeToken(row, col, playerToken);
			
			// If it's not placed, asks the user to do it again
			while(!placed) {
				
				System.out.println("Please enter valid inputs!!! \n");
				
				System.out.println("Please enter a row (1-3): ");
				row = Integer.parseInt(sc.nextLine()) - 1;
			
				System.out.println("Please enter a column (1-3): ");
				col = Integer.parseInt(sc.nextLine()) - 1;
				
				placed = board.placeToken(row, col, playerToken);
				
			}
			
			if(board.checkIfWinning(playerToken)) {
				
				System.out.println(board);
				
				System.out.println("Player " + player + " is the winner!");
				gameOver = true;
				
			}
			
			else if(board.checkIfFull()) {
				
				System.out.println("Its a tie!");
				gameOver = true;
				
			}
			
			else {
				
				player++;
				
				if(player > 2) {
					
					player = 1;
					
				}
				
			}
			
		}
		
	}
	
}