public enum Tile {
	
	BLANK("_"),
	X("X"),
	O("O");
	
	private final String name;
	
	private Tile(String name) {
		
		this.name = name;
		
	}
	
	// Returns the name of the tile
	public String getName() {
		
		return this.name;
		
	}
	
}